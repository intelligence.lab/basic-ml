

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Assignment {
	//how many times the learning program should run 
	int runCount;
	//the number of attributes in each datagrid row
	int number_of_attributes = 9;
	//represents each row on the given datagrid input
	DataGridRow[] dataSet;
	int TOTAL_DATAGRID_ROWS = 683;
	NodeAttribute rootNode;
	boolean[] isChecked = new boolean[number_of_attributes];
	int trainingDataSize;

	class DataGridRow {
		//each row has 9 atrributes with specific values
		int attributes[];
		//the class to it belongs; 0/1 for our example
		int theClass;

		public DataGridRow() {
			attributes = new int[number_of_attributes];
			theClass = 0;
		}

		public void takeInput(final Scanner sc) {
			for (int i = 0; i < number_of_attributes; i++) {
				//the way we are taking the values is 0-9,which is 1-10 actually
				attributes[i] = sc.nextInt() - 1; 
			}

			theClass = sc.nextInt();//the last column defines the class
		}
	}

	class NodeAttribute {
		//the dataset valid for this node
		public ArrayList<Integer> subsetOfExamples;
		public int size;
		//based on 10 distinct
		public NodeAttribute[] child;
		public int nextRootAttribute;
		public boolean isLeaf;
		public int theClass;
		public double entropy;
		int number_of_children = 10;//each attribute can have 10 distinct values [0-9]
		

		public NodeAttribute() {
			child = new NodeAttribute[number_of_children];
			isLeaf = false;
			theClass = -1;
			nextRootAttribute = -1;
		}

		public void setExamples(ArrayList<Integer> examples) {
			this.subsetOfExamples = examples;
			this.size = examples.size();
		}

		public void takeDesicion() {


			if (subsetOfExamples.size() == 0) {
				theClass = 0; // the best class is 0 for given data,so we decide this
				isLeaf = true;
				return;
			}

			entropy = entropy(subsetOfExamples);

			if (entropy != 0) {
				double gain = -1, temp = 0;
				int maxAttr = -1;
				//here we find out which attribute is we are going to select as root
				for (int i = 0; i < number_of_attributes; i++) {
					if (!isChecked[i]) {
						temp = gain(entropy, subsetOfExamples, i,number_of_children);
						//ensuring max gain attribute to be the root
						if (temp > gain) {
							gain = temp;
							maxAttr = i;
						}
					}
				}
				isChecked[maxAttr] = true;//this helps us to overlook this attribute next time
				nextRootAttribute = maxAttr;


				for (int i = 0; i < number_of_children; i++) {
					child[i] = new NodeAttribute();
					child[i].setExamples(getSubExampleList(subsetOfExamples, maxAttr, i));
					child[i].takeDesicion();
				}

				isChecked[maxAttr] = false;
			}

			else {
				isLeaf = true;
				theClass = dataSet[subsetOfExamples.get(0)].theClass;
			}
		}
	}

	/**
	 * The subset from given example valid for a step.
	 * @param list
	 * @param attrib_num
	 * @param attrib_val
	 * @return
	 */
	public ArrayList<Integer> getSubExampleList(ArrayList<Integer> list,
			int attrib_num, int attrib_val) {
		ArrayList<Integer> ret_list = new ArrayList<Integer>();
		for (Integer index : list) {
			if (dataSet[index].attributes[attrib_num] == attrib_val) {
				ret_list.add(index);
			}
		}
		return ret_list;
	}

	public double gain(double entropy, ArrayList<Integer> list,
			int attr_num, int noOfChildren) {
		double temp_sum = 0;
		ArrayList<Integer> subsetOfExamples;

		for (int i = 0; i < noOfChildren; i++) {
			subsetOfExamples = getSubExampleList(list, attr_num, i);

			if (subsetOfExamples.size() != 0) {
				temp_sum += ((double) subsetOfExamples.size() / (double) list
						.size()) * entropy(subsetOfExamples);

			}
		}

		return entropy - temp_sum;
	}

	public double entropy(ArrayList<Integer> list) {
		//number of positive and negative decisions
		double pos = 0;
		double neg = 0;
		double retVal = 0;

		for (Integer index : list) {
			if (dataSet[index].theClass == 1) {
				pos++;
			} else
				neg++;
		}

		if (neg == 0 || pos == 0)
			return 0;

		retVal = -((pos / list.size()) * ((Math.log(pos / list.size())) / (Math
				.log(2))))
				- ((neg / list.size()) * ((Math.log(neg / list.size())) / (Math
						.log(2))));
		return retVal;
	}

	private void takeInput() throws FileNotFoundException {
		Scanner sc = new Scanner(new FileInputStream(new File("data.txt")));

		dataSet = new DataGridRow[TOTAL_DATAGRID_ROWS];

		for (int i = 0; i < TOTAL_DATAGRID_ROWS; i++) {
			dataSet[i] = new DataGridRow();
			dataSet[i].takeInput(sc);
		}

	}

	public boolean checkData(NodeAttribute n, DataGridRow d) {
		if (n.isLeaf) {
			return n.theClass == d.theClass;
		}

		return checkData(n.child[d.attributes[n.nextRootAttribute]], d);
	}

	public void run(int rntms) throws Exception {


		runCount = rntms;
		double correct = 0, incorrect = 0;
		double true_pos, true_neg, false_pos, false_neg;

		double precision, accuracy,recall;
		double avg_precision = 0,avg_accuracy = 0,avg_recall=0;


		takeInput();
		trainingDataSize = (int) ((double) TOTAL_DATAGRID_ROWS * 0.8);

		ArrayList<Integer> tempList = new ArrayList<Integer>();
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < TOTAL_DATAGRID_ROWS; i++)
			list.add(i);

		
		for (int rc = 0; rc < runCount; rc++) {

			rootNode = new NodeAttribute();

			// populate random data list
			tempList.clear();
			for (int i = 0; i < trainingDataSize; i++)
				tempList.add(list.get(i));

			// set the examples at root node
			rootNode.setExamples(tempList);

			//decision tree making algorithm
			rootNode.takeDesicion();

			/*test data */
			correct = 0;
			incorrect = 0;
			true_neg = true_pos = false_neg = false_pos = 0;

			for (int i = trainingDataSize; i < TOTAL_DATAGRID_ROWS; i++) {
				if (checkData(rootNode, dataSet[list.get(i)])) {
					correct++;
					if (dataSet[list.get(i)].theClass == 0) {
						true_neg++;//negative decision was made and its true
					} else {
						true_pos++;//positive decision was made and its true
					}
				} else {
					incorrect++;
					if (dataSet[list.get(i)].theClass == 0) {
						false_pos++;//positive decision was made but its false
					} else {
						false_neg++;//negative decision was made but its false
					}
				}
			}

			// calculate precision
			//number of positives that I guessed correctly
			precision = true_pos / (true_pos + false_pos);
			avg_precision += precision;


			// calculate accuracy
			accuracy = (true_pos + true_neg)
					/ (true_pos + true_neg + false_pos + false_neg);
			avg_accuracy += accuracy;
			//calculate recall
			recall = true_pos/(true_pos+false_neg);
			avg_recall +=recall;

			//java mthod to randomize collections
			Collections.shuffle(list);
		}

		System.out.println("accuracy(avg. based on how many times it ran) "
				+ (avg_accuracy * 100 / runCount));
		System.out.println("precision(avg. based on how many times it ran) "
				+ (avg_precision * 100 / runCount));
		System.out.println("recall(avg. based on how many times it ran) "
				+ (avg_recall * 100 / runCount));
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		int rntms =100;
		System.out.println("Running for:"+rntms+" times...");
		Assignment ass1= new Assignment();
		ass1.run(rntms);
	}
}
