import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.helpers.CSVFileReader;
import com.helpers.ColumnValue;
import com.helpers.Engine;
import com.helpers.WeakLearner;

public class Launcher {
	
	
	
	public static void main(String []args) {
		
		//A ColumnValue defines a column and a list of ColumnValue means a row
		//Records is a list of all the rows
		List<List<ColumnValue>> Records= new ArrayList<List<ColumnValue>>();
		int numberOfLearners= 1;
		ArrayList<WeakLearner> weakLearners = new ArrayList<WeakLearner>();
		
		CSVFileReader reader=new CSVFileReader();
		reader.readFile(Records);
		
		Engine engine = new Engine(Records);
		
		weakLearners.add(engine.adaBoost(engine.Records, engine.Weights));
		List<List<ColumnValue>> SampledSet = engine.getTrainingDataBasedOnProbability(engine.Weights); 
		
		for(int i=1;i<numberOfLearners;i++)
		{
			WeakLearner c = engine.adaBoost(SampledSet, engine.Weights);
			weakLearners.add(c);
			SampledSet = engine.getTrainingDataBasedOnProbability(engine.Weights);
		}
		
		int correctDetections = 0;
		for(int i=0; i<Records.size(); i++)
		{
			ArrayList<Integer> testData=Engine.getRowData(Records.get(i));
			int realResult = testData.get(testData.size()-1);
			if(realResult==0) realResult=-1;
			double predictedResult = engine.predictByBoosting(weakLearners, testData);
			
			if(realResult == predictedResult) correctDetections++;
			
		}
		
		System.out.println("Detected:"+String.valueOf(correctDetections)+" out of " + Records.size());
		System.out.println("Accuracy:"+correctDetections*100.0f/Records.size()*1.0f+" %");
		
		
		//generating data set randomly
		List<List<ColumnValue>> trainingSet= new ArrayList<List<ColumnValue>>();
		List<List<ColumnValue>> testSet= new ArrayList<List<ColumnValue>>();
		
		
		ArrayList<Integer> trainingCount = new ArrayList<Integer>();
		ArrayList<Integer> testCount = new ArrayList<Integer>();

		int numberOfRecords = Records.size();
		int trainingDataSize = (int) (numberOfRecords*(.8));
	
		
		Random randomGenerator = new Random(); 
		
		while (correctDetections < trainingDataSize) {
			int ran = randomGenerator.nextInt(numberOfRecords);
			if (!trainingCount.contains(new Integer(ran))) {
				trainingCount.add(new Integer(ran));
			}
		}
		
		Collections.sort(trainingCount);

		
		for (int newCount = 0; newCount < numberOfRecords; newCount++) {
			if (!trainingCount.contains(new Integer(newCount)))
				testCount.add(newCount);
		}
		
		Collections.sort(testCount);
		
		for(int i=0;i<testCount.size();i++)
			testSet.add(Records.get(testCount.get(i)));
		
		for(int i=0;i<trainingCount.size();i++)
			trainingSet.add(Records.get(trainingCount.get(i)));
		
	}
	
	


	
}
