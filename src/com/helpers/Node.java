package com.helpers;
import java.util.ArrayList;
import java.util.List;


public class Node {
	
	public int attribute;
	public int value;
	Node[] Children;
	
	public Node()
	{
		Children = new Node[10];
	}
	
	public Node(int attribute,int value)
	{
		this.attribute = attribute;	
		this.value = value;
		Children = new Node[10];
	}
	
	public void addChildren(int i,Node child)
	{
		Children[i] = child;
	}
	
	
}
