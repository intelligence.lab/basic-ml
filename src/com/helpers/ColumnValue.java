package com.helpers;
public class ColumnValue {
	public int columnIndex;
	public int value;
	
	public ColumnValue(int columnIndex, int value)
	{
		this.columnIndex=columnIndex;
		this.value=value;
	}
}
