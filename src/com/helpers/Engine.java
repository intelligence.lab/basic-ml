package com.helpers;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class Engine {
	
	public List<List<ColumnValue>> Records;
	public ArrayList<Double> Weights = new ArrayList<Double>();
	public ArrayList<Integer> columns = new ArrayList<Integer>(Arrays.asList(1,1,1,1,1,1,1,1,1));
	Random r = new Random();
	
	public double predictByBoosting(ArrayList<WeakLearner> weakLearners,List<Integer> testData)
	{
		Double sum=0.0;
		testData.remove(testData.size()-1);
		
		for(int i=0;i<weakLearners.size();i++)
		{
			int predictedResult = walkTree(weakLearners.get(i).node,testData);
			if(predictedResult==0) predictedResult=-1;
			sum+=predictedResult*weakLearners.get(i).weight;
		}
		
		if(sum>0) return 1;
		else return -1;
	}
	
	public static  ArrayList<Integer> getRowData(List<ColumnValue> x)
	{
		ArrayList<Integer> values = new ArrayList<Integer>();
		
		for(int i=0;i<x.size();i++)
			values.add(x.get(i).value);
		
		return values;
	}
	
	public static int walkTree(Node root,List<Integer> row)
	{
		if(root.attribute==-1)
			return root.value;
		
		return walkTree(root.Children[row.get(root.attribute)-1],row);
	}
	
	/**
	 * Initial Construction
	 * @param Records
	 */
	public Engine(List<List<ColumnValue>> Records)
	{
		this.Records = Records;
		 		
		for(int i=0;i<Records.size();i++)
			Weights.add(1.0);
	    for(int i=0;i<Records.size();i++)
	    {
	    	Weights.set(i, (double)(Weights.get(i)/Weights.size()));
	    }
	}
	
	public void copyIntegerList(List<Integer> a,List<Integer> b)
	{
		for(int i=0;i<b.size();i++)
		{
			Integer value = b.get(i);
			a.add(value);
		}
	}
	
	public List<List<ColumnValue>> getTrainingDataBasedOnProbability(List<Double> weights)
    {
        List<List<ColumnValue>> trainingData = new ArrayList<List<ColumnValue>>();
        List<Double> tempWeights = new ArrayList<Double>();
        double sumOfWeights=0;
        
        for(int i = 0;i<weights.size();i++) sumOfWeights += weights.get(i);
        
        for (int i = 0; i < Records.size(); i++)
        {
            double sum = 0;
            for(int j=0;j<=i;j++)
                sum += weights.get(j);
            
            tempWeights.add(sum/sumOfWeights);
        }
        
        for (int i = 0; i < Records.size(); i++)
        {
        	double range = r.nextDouble(); 
        	int index = 0;
            for (int j = 0; j < tempWeights.size()-1; j++)
            {
                if (range > tempWeights.get(j) && range < tempWeights.get(j+1))
                {
                    index =j;
                    break;
                }
            }
            
            trainingData.add(Records.get(index));
            
        }
        
        return trainingData;
    }
	
	public WeakLearner adaBoost(List<List<ColumnValue>> SampledRecords,ArrayList<Double> Weights)
	{
		ArrayList<Integer> PredictedResults = new ArrayList<Integer>();
		double sumOfWeights = 0;
		
		for(int i = 0;i<Weights.size();i++) sumOfWeights += Weights.get(i);
		
		DecisionStamp id=new DecisionStamp();
		Node root = id.modifiedID3(SampledRecords, columns);
		
		WeakLearner learner = new WeakLearner(root,1.0);
		double errorVector = 0;
		
		for(int i=0; i<Records.size(); i++)
		{
			ArrayList<Integer> testData=new ArrayList<Integer>();
			copyIntegerList(testData,Engine.getRowData(Records.get(i)));
			int realResult = testData.get(testData.size()-1);
			testData.remove(testData.size()-1);//remove the result column
			int predictedResult = Engine.walkTree(root,testData);
			if(realResult!=predictedResult)
				errorVector+=Weights.get(i);
			PredictedResults.add(predictedResult);
		}
		
		//updating learner weight
		learner.weight = ((float)(1-errorVector))/errorVector;
		learner.weight = .5*log(learner.weight);
		
		//updating sample weights
		for(int i=0; i<Records.size(); i++)
		{
			ArrayList<Integer> testData=new ArrayList<Integer>();
			copyIntegerList(testData,Engine.getRowData(Records.get(i)));
			int realResult = testData.get(testData.size()-1);
			int predictedResult = PredictedResults.get(i);
			if(realResult==0) realResult=-1;
			if(predictedResult==0) predictedResult=-1;
			double exp = learner.weight*realResult*predictedResult*-1;
			exp = Math.exp(exp);
			double newWeight =  Weights.get(i)*exp;
			newWeight = ((float)(newWeight))/sumOfWeights;
			Weights.set(i, newWeight);
		}
		
		return learner;
		
	}
	
	//CHECKED
	public int getMostCommonValue(List<List<ColumnValue>> Records)
	{
		int maxfrequency=0;
		int mostCommonVal=-1;
		ArrayList<Integer> values = new ArrayList<Integer>();
		values = getAllValuesForAttribute((Records.get(0)).size()-1);
		
		for(int i=0;i<values.size();i++)
		{
			int frequency=0;
			for(int j = 0; j<Records.size(); j++)
			{
				List<ColumnValue> rowValue = Records.get(j);
				int result = (rowValue.get(rowValue.size()-1)).value;
				if(result==values.get(i)) frequency++; 				
			}
			
			if(frequency>maxfrequency)
			{
				maxfrequency=frequency;
				mostCommonVal=values.get(i);
			}
		}
		
		return mostCommonVal;
	}
	
	public boolean isAllPositive()
	{
		for(int i = 0; i<Records.size(); i++)
		{
			List<ColumnValue> rowValue = Records.get(i);
			int result = (rowValue.get(rowValue.size()-1)).value;
			if(result == 0) return false;			
		}
		
		return true;
	}
	
	public boolean isAllNegative()
	{
		for(int i = 0; i<Records.size(); i++)
		{
			List<ColumnValue> rowValue = Records.get(i);
			int result = (rowValue.get(rowValue.size()-1)).value;
			if(result == 1) return false;			
		}
		
		return true;
	}
	
	public float getTotalEntropy()
	{
		float entropy = 0;
		int totalRows = Records.size();
		int positiveCount = 0;
		int negativeCount = 0;
		
		
		for(int i = 0; i<Records.size(); i++)
		{
			List<ColumnValue> rowValue = Records.get(i);
			int value = (rowValue.get(rowValue.size()-1)).value;
			if(value == 1) positiveCount++ ;
			else negativeCount++;				
		}
		
		if(positiveCount!=0) entropy = (((float)positiveCount)/totalRows)*log(((float)positiveCount)/totalRows);
		else entropy=0;
		if(negativeCount!=0) entropy += (((float)negativeCount)/totalRows)*log(((float)negativeCount)/totalRows);
		
		entropy *= -1;
		
		return entropy;
	}
	
	public float getEntropyAndProbability(int columnIndex, int value)
	{
		float entropy = 0;
		int totalRows = 0;
		int positiveCount = 0;
		int negativeCount = 0;
		
		for(int i = 0; i<Records.size(); i++)
		{
			List<ColumnValue> rowValue = Records.get(i);
			int dataValue = (rowValue.get(columnIndex)).value;
			
			if(dataValue == value) 
			{
				totalRows++;
				int resultVal = (rowValue.get(rowValue.size()-1)).value;
				if(resultVal == 1) positiveCount++ ;
				else negativeCount++;
			}
		}
		
		if(positiveCount!=0) entropy = (((float)positiveCount)/totalRows)*log(((float)positiveCount)/totalRows);
		else entropy=0;
		if(negativeCount!=0) entropy += (((float)negativeCount)/totalRows)*log(((float)negativeCount)/totalRows);
		
		entropy *= -1;
		
		return entropy*(((float)totalRows)/Records.size());
	}
	
	public float getGain(int attribute)
	{
		float gain = 0;
		float subtract = 0;
		
		ArrayList<Integer> values = new ArrayList<Integer>();
		values = getAllValuesForAttribute(attribute);
		
		for(int i = 0; i<values.size(); i++)
			subtract += getEntropyAndProbability(attribute,values.get(i));
		
		gain = getTotalEntropy() - subtract;
		return gain;
	}
	
	public float misClassificationImpurity(int attribute)
	{
		ArrayList<Float> probabilities=new ArrayList<Float>();
		ArrayList<Integer> values = getAllValuesForAttribute(attribute);
		int maxfrequency=0;
		
		for(int i=0;i<values.size();i++)
		{
			int frequency=0;
			for(int j = 0; j<Records.size(); j++)
			{
				List<ColumnValue> rowValue = Records.get(j);
				int value = (rowValue.get(rowValue.size()-1)).value;
				if(value==values.get(i)) frequency++; 				
			}
			
			if(frequency>maxfrequency)
			{
				maxfrequency=frequency;
			}
		}
		
		return 1-(maxfrequency/Records.size());
		
	}
	
	float log(double x)
	{
		return (float) (Math.log(x)/Math.log(2)+1e-10);
	}
	
	//CHECKED
	public ArrayList<Integer> getAllValuesForAttribute(int attribute)
	{
		ArrayList<Integer> values = new ArrayList<Integer>();
		
		for(int i = 0; i<Records.size(); i++)
		{
			List<ColumnValue> rowValue = Records.get(i);
			Integer val = rowValue.get(attribute).value;
			//val is not in the list
			if(values.indexOf(val)==-1) values.add(val);
		}
		
		return values;
	}
	
	public void cutDataSet(int attribute,int value,List<List<ColumnValue>> Records)
	{
		int i = 0;
		
		while(i<Records.size())
		{
			List<ColumnValue> rowValue = Records.get(i);
			Integer val = rowValue.get(attribute).value;
			
			if(val!=value) Records.remove(i);
			else i++;
		}
	}
	
	public int getBestAttribute(List<Integer> columns)
	{
		int bestAttr = 0;
		float maxGain = -5000;
		
		for(int i=0;i<columns.size();i++)
		{
			if(columns.get(i)==1)
			{
				float gain = getGain(i);
				if (gain>maxGain)
				{
					maxGain=gain;
					bestAttr=i;
				}
				
			}
		}
		
		return bestAttr;
	}
}
