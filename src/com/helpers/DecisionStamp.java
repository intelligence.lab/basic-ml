package com.helpers;
import java.util.ArrayList;
import java.util.List;


public class DecisionStamp {
	
	
	public Node modifiedID3(List<List<ColumnValue>> Records, List<Integer> attributes)
	{
		Node root = new Node();
		Engine helper = new Engine(Records);
		
		if(helper.isAllPositive()) return new Node(-1,1);
		if(helper.isAllNegative()) return new Node(-1,0);
		
		/*if(attributes.size()==0)
		{
			return root = new Node(-1,helper.getMostCommonValue());
		}*/
		
		int v;
		for(v=0;v<attributes.size();v++)
		{
			if(attributes.get(v)!=0) break; 
		}
		
		if(v==attributes.size()) return root = new Node(-1,helper.getMostCommonValue(helper.Records));
		
		helper = new Engine(Records);
		int bestAttr = helper.getBestAttribute(attributes);
		root.attribute = bestAttr;
		
		//attributes.set(bestAttr, 0);
		
		for(int i=0; i<10; i++)
		{
			List<List<ColumnValue>> recordsSubset = new ArrayList<List<ColumnValue>>();
			copyDataset(recordsSubset,Records);
			helper.cutDataSet(bestAttr, i, recordsSubset);
			
			if(recordsSubset.size()!=0) root.addChildren(i, new Node(-1, helper.getMostCommonValue(recordsSubset)));
			else root.addChildren(i, new Node(-1,helper.getMostCommonValue(helper.Records)));
			
		}
		
		return root;
	}
	
	public void copyDataset(List<List<ColumnValue>> a,List<List<ColumnValue>> b)
	{
		for(int i=0;i<b.size();i++)
		{
			List<ColumnValue> row=new ArrayList<ColumnValue>();
			row = b.get(i);
			a.add(row);
		}
	}
	
	public void copyIntegerList(List<Integer> a,List<Integer> b)
	{
		for(int i=0;i<b.size();i++)
		{
			Integer value = b.get(i);
			a.add(value);
		}
	}
}
