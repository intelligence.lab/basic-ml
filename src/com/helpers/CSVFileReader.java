package com.helpers;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CSVFileReader {
	
	public void readFile(List<List<ColumnValue>> Records)
	{
		String csvFile = "data.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		try {
			 
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
	 
				String[] values = line.split(cvsSplitBy);
				List<ColumnValue> row = new ArrayList<ColumnValue>();
				
				for(int i = 0; i<values.length; i++) 
					row.add(new ColumnValue(i,Integer.parseInt(values[i])));
				
				Records.add(row);
			}
			
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	  
	}
	
	public void printData(List<List<ColumnValue>> Records)
	{
		for(int i=0;i<Records.size();i++)
		{
			List<ColumnValue> row = new ArrayList<ColumnValue>();
			row=Records.get(i);
			String s = "";
			
			for(int j=0; j<row.size();j++)
			{
				s += String.valueOf(((row.get(j)).value)) + ",";
			}
			
			System.out.println(s);
			
		}
		
	}
	
}

